import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  constructor(
    private navCtrl: NavController,
    private userSrvc: UserService,
  ) { }

  ngOnInit() {
  }

  navProfile() {
    this.navCtrl.navigateForward('/profile');
  }

  navPage(url: string) {
    this.navCtrl.navigateForward(url);
  }

  dashboardToggleChanged(event) {
    this.userSrvc.dashboardToggle = event.detail.value;
  }
}
