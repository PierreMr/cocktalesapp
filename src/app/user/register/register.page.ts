import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ControllersService } from 'src/app/services/controllers/controllers.service';
import { NavController, ModalController } from '@ionic/angular';
import { UserService } from 'src/app/services/user/user.service';
import { api } from 'src/environments/environment';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  registerForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private ctrlSrvc: ControllersService,
    private navCtrl: NavController,
    private modalCtrl: ModalController,
    private userSrvc: UserService,
  ) {
    this.registerForm = this.formBuilder.group({
      firstname: ['Pierre'],
      lastname: ['Mauer'],
      email: ['mauerpierre@gmail.com'],
      password: ['123456'],
      phone: ['0680631639'],
      address: ['47 avenue du Docteur Arnold Netter'],
      zip: ['75012'],
      city: ['Paris'],
      image: ['default.png'],
      isEmployer: [false],
    });
  }

  ngOnInit() {
  }

  onSubmit(registerForm) {
    this.ctrlSrvc.presentLoading('Chargement...')
    .then(() => {
      const user = registerForm.value;
      user.status = true;
      
      const init = { 
        method: "POST",
        body: JSON.stringify(user),
      };
      const url = api.url+'/user/new';

      fetch(url, init)
      .then(response => {
        response.json()
        .then(user => {
          if (user.error) {
            console.log(user);
            if (user.message) this.ctrlSrvc.presentToast(`${user.message}`, 3000, 'danger');
            if (user.error.violations) {
              let message = "";
              user.error.violations.forEach(violation => {
                message += violation.reason + ". ";
              });
              this.ctrlSrvc.presentToast(`${message}`, 3000, 'danger');
            }
          } else {
            this.ctrlSrvc.presentToast(`Inscription réussi.`, 3000, 'primary');
            this.navCtrl.navigateRoot('/login');
          }
          this.ctrlSrvc.loadingCtrl.dismiss();
        }).catch(error => {
          console.log(error);
          this.ctrlSrvc.loadingCtrl.dismiss();
        });
      }).catch((error) => {
        console.log(error);
        this.ctrlSrvc.loadingCtrl.dismiss();
      })
    });
  }

}
