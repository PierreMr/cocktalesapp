import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { UserCompaniesPage } from './user-companies.page';

const routes: Routes = [
  {
    path: '',
    component: UserCompaniesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [UserCompaniesPage]
})
export class UserCompaniesPageModule {}
