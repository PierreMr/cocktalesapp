import { Component, OnInit } from '@angular/core';
import { Company } from 'src/app/models/company.interface';
import { UserService } from 'src/app/services/user/user.service';
import { api } from 'src/environments/environment';
import { NavController } from '@ionic/angular';
import { ControllersService } from 'src/app/services/controllers/controllers.service';

@Component({
  selector: 'app-user-companies',
  templateUrl: './user-companies.page.html',
  styleUrls: ['./user-companies.page.scss'],
})
export class UserCompaniesPage implements OnInit {
  allCompanies;
  companies;

  constructor(
    private userSrvc: UserService,
    private navCtrl: NavController,
    private ctrlSrvc: ControllersService,
  ) {
    this.ctrlSrvc.presentLoading('')
    .then(() => {
      this.getCompanies()
      .then(companies => {
        this.allCompanies = companies;
        this.companies = companies;
        this.ctrlSrvc.loadingCtrl.dismiss();
      }).catch(error => {
        console.log(error);
        this.ctrlSrvc.loadingCtrl.dismiss();
      });
    });
  }

  ngOnInit() {
  }

  getCompanies() {
    return new Promise((resolve, reject) => {
      if (this.userSrvc.companies.length === 0) {
        const headers = new Headers();
        headers.append("Authorization", this.userSrvc.apiToken);
        const init = { 
          method: 'GET',
          headers: headers,
        };
        const url = api.url+'/company/user/'+this.userSrvc.user.id;

        fetch(url, init)
        .then(response => {
          response.json()
          .then(companies => {
            console.log(companies);
            this.userSrvc.companies = companies;
            resolve(companies);
          }).catch(error => {
            console.log(error);
            reject(error);
          });
        }).catch(error => {
          console.log(error);
          reject(error);
        });
      }
      else {
        resolve(this.userSrvc.companies);
      }
    });
  }

  doRefresh(event) {
    this.getCompanies()
    .then(companies => {
      this.allCompanies = companies;
      this.companies = companies;
      event.target.complete();
    }).catch(error => {
      console.log(error);
      event.target.complete();
    });
  }

  navCompanyDetail(id: number) {
    this.navCtrl.navigateForward(['/company-detail/'+id]);
  }

  onCompanyChange(input: string): void {
    const companyInput = input.toLowerCase();
    this.companies = this.allCompanies.filter(company =>
        company.name.toLowerCase().includes(companyInput)
    );
  }

  onCompanyCancel() {
    this.companies = this.allCompanies;
  }

  navAddCompany() {
    this.navCtrl.navigateForward('/add-company');
  }

}
