import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  user;

  constructor(
    private navCtrl: NavController,
    private userSrvc: UserService,
  ) {
    this.user = this.userSrvc.user;
  }

  ngOnInit() {
  }

  logout() {
    this.userSrvc.logout();
  }

  navNoWhere() {
    console.log('NavNoWhere');
  }
}
