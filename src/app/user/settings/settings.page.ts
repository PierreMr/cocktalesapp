import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  user;

  constructor(
    private navCtrl: NavController,
    private userSrvc: UserService,
  ) {
    this.user = this.userSrvc.user;
  }

  ngOnInit() {
  }

  navProfile() {
    this.navCtrl.navigateForward('/profile');
  }

  navNoWhere() {
    console.log('NavNoWhere');
  }
}
