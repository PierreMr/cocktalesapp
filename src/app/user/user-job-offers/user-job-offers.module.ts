import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { UserJobOffersPage } from './user-job-offers.page';

const routes: Routes = [
  {
    path: '',
    component: UserJobOffersPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [UserJobOffersPage]
})
export class UserJobOffersPageModule {}
