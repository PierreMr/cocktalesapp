import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';
import { NavController } from '@ionic/angular';
import { ControllersService } from 'src/app/services/controllers/controllers.service';
import { api } from 'src/environments/environment';

@Component({
  selector: 'app-user-job-offers',
  templateUrl: './user-job-offers.page.html',
  styleUrls: ['./user-job-offers.page.scss'],
})
export class UserJobOffersPage implements OnInit {
  allUserJobOffers;
  userJobOffers;

  constructor(
    private userSrvc: UserService,
    private navCtrl: NavController,
    private ctrlSrvc: ControllersService,
  ) {
    this.ctrlSrvc.presentLoading('')
    .then(() => {
      this.getJobOffers()
      .then(jobOffers => {
        this.allUserJobOffers = jobOffers;
        this.userJobOffers = jobOffers;
        this.ctrlSrvc.loadingCtrl.dismiss();
      }).catch(error => {
        console.log(error);
        this.ctrlSrvc.loadingCtrl.dismiss();
      });
    });
  }

  ngOnInit() {
  }

  getJobOffers() {
    return new Promise((resolve, reject) => {
      if (this.userSrvc.userJobOffers.length === 0) {
        const headers = new Headers();
        headers.append("Authorization", this.userSrvc.apiToken);
        const init = { 
          method: 'GET',
          headers: headers,
        };
        const url = api.url+'/user/joboffers/'+this.userSrvc.user.id;

        fetch(url, init)
        .then(response => {
          response.json()
          .then(userJobOffers => {
            console.log(userJobOffers);
            this.userSrvc.userJobOffers = userJobOffers;
            resolve(userJobOffers);
          }).catch(error => {
            console.log(error);
            reject(error);
          });
        }).catch(error => {
          console.log(error);
          reject(error);
        });
      }
      else {
        resolve(this.userSrvc.userJobOffers);
      }
    });
  }

  doRefresh(event) {
    this.getJobOffers()
    .then(jobOffers => {
      this.allUserJobOffers = jobOffers;
      this.userJobOffers = jobOffers;
      event.target.complete();
    }).catch(error => {
      console.log(error);
      event.target.complete();
    });
  }

  navUserJobOfferDetail(id: number) {
    this.navCtrl.navigateForward(['/job-offer-detail/'+id]);
  }

  onJobOfferChange(input: string): void {
    const jobOfferInput = input.toLowerCase();
    this.userJobOffers = this.allUserJobOffers.filter(jobOffer =>
        jobOffer.title.toLowerCase().includes(jobOfferInput)
    );
  }

  onJobOfferCancel() {
    this.userJobOffers = this.allUserJobOffers;
  }

}
