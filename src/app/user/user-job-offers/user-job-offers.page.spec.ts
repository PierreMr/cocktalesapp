import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserJobOffersPage } from './user-job-offers.page';

describe('UserJobOffersPage', () => {
  let component: UserJobOffersPage;
  let fixture: ComponentFixture<UserJobOffersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserJobOffersPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserJobOffersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
