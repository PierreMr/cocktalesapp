import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ControllersService } from 'src/app/services/controllers/controllers.service';
import { api } from 'src/environments/environment';
import { UserService } from 'src/app/services/user/user.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;

  constructor(
    private ctrlSrvc: ControllersService,
    private userSrvc: UserService,
    private formBuilder: FormBuilder,
    private navCtrl: NavController,
  ) {
    this.loginForm = this.formBuilder.group({
      email: [],
      password: [],
    });
  }

  ngOnInit() {}

  onSubmit(loginForm) {
    const login = {
      email: loginForm.value.email, 
      password: loginForm.value.password
    };

    fetch(api.url+'/login', {
      method: "POST",
      body: JSON.stringify(login)
    })
    .then(response => {
      response.json()
      .then(user => {
        if (user.error) {
          this.ctrlSrvc.presentToast(`${user.message}`, 3000, 'danger');
        } else {
          this.userSrvc.login(user);
          this.ctrlSrvc.presentToast(`Vous êtes connecté.`, 3000, 'primary');
          this.navCtrl.navigateRoot('/home');
        }
      }).catch((error) => {
        console.log(error);
      });
    }).catch((error) => {
      console.log(error);
    });
  }
}
