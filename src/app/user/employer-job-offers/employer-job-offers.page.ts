import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';
import { NavController } from '@ionic/angular';
import { ControllersService } from 'src/app/services/controllers/controllers.service';
import { api } from 'src/environments/environment';

@Component({
  selector: 'app-employer-job-offers',
  templateUrl: './employer-job-offers.page.html',
  styleUrls: ['./employer-job-offers.page.scss'],
})
export class EmployerJobOffersPage implements OnInit {
  allJobOffers;
  jobOffers;

  constructor(
    private userSrvc: UserService,
    private navCtrl: NavController,
    private ctrlSrvc: ControllersService,
  ) { 
    this.ctrlSrvc.presentLoading('')
    .then(() => {
      this.getJobOffers()
      .then(jobOffers => {
        this.allJobOffers = jobOffers;
        this.jobOffers = jobOffers;
        this.ctrlSrvc.loadingCtrl.dismiss();
      }).catch(error => {
        console.log(error);
        this.ctrlSrvc.loadingCtrl.dismiss();
      });
    });
  }

  ngOnInit() {
  }

  getJobOffers() {
    return new Promise((resolve, reject) => {
      if (this.userSrvc.jobOffers.length === 0) {
        const headers = new Headers();
        headers.append("Authorization", this.userSrvc.apiToken);
        const init = { 
          method: 'GET',
          headers: headers,
        };
        const url = api.url+'/joboffer/user/'+this.userSrvc.user.id;

        fetch(url, init)
        .then(response => {
          response.json()
          .then(jobOffers => {
            this.userSrvc.jobOffers = jobOffers;
            resolve(jobOffers);
          }).catch(error => {
            console.log(error);
            reject(error);
          });
        }).catch(error => {
          console.log(error);
          reject(error);
        });
      }
      else {
        resolve(this.userSrvc.jobOffers);
      }
    });
  }

  doRefresh(event) {
    this.getJobOffers()
    .then(jobOffers => {
      this.allJobOffers = jobOffers;
      this.jobOffers = jobOffers;
      event.target.complete();
    }).catch(error => {
      console.log(error);
      event.target.complete();
    });
  }

  navJobOfferDetail(id: number) {
    this.navCtrl.navigateForward(['/job-offer-detail/'+id]);
  }

  onJobOfferChange(input: string): void {
    const jobOfferInput = input.toLowerCase();
    this.jobOffers = this.allJobOffers.filter(jobOffer =>
        jobOffer.title.toLowerCase().includes(jobOfferInput)
    );
  }

  onJobOfferCancel() {
    this.jobOffers = this.allJobOffers;
  }

  navAddJobOffer() {
    this.navCtrl.navigateForward('/add-job-offer');
  }

}
