import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerJobOffersPage } from './employer-job-offers.page';

describe('EmployerJobOffersPage', () => {
  let component: EmployerJobOffersPage;
  let fixture: ComponentFixture<EmployerJobOffersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployerJobOffersPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployerJobOffersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
