import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';
import { UserService } from './services/user/user.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Tableau de bord',
      url: '/dashboard',
      icon: 'desktop'
    },
    {
      title: 'News',
      url: '/home',
      icon: 'albums'
    },
    {
      title: 'Contact',
      url: '/home',
      icon: 'contact'
    },
  ];
  
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: Storage,
    private userSrvc: UserService,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.storage.get('connected').then((connected) => {
        if (connected) {
          this.storage.get('apiToken').then((apiToken) => {
            this.userSrvc.apiToken = apiToken;
            this.storage.get('user').then((user) => {
              this.userSrvc.user = user;
            });
          });
        }
      });
    });
  }
}
