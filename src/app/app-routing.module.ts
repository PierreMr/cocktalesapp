import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'login', loadChildren: './user/login/login.module#LoginPageModule' },
  { path: 'profile', loadChildren: './user/profile/profile.module#ProfilePageModule' },
  { path: 'user-companies', loadChildren: './user/user-companies/user-companies.module#UserCompaniesPageModule' },
  { path: 'add-company', loadChildren: './company/add-company/add-company.module#AddCompanyPageModule' },
  { path: 'list-job-offers', loadChildren: './job-offer/list-job-offers/list-job-offers.module#ListJobOffersPageModule' },
  { path: 'job-offer-detail/:id', loadChildren: './job-offer/job-offer-detail/job-offer-detail.module#JobOfferDetailPageModule' },
  { path: 'add-job-offer', loadChildren: './job-offer/add-job-offer/add-job-offer.module#AddJobOfferPageModule' },
  { path: 'company-detail/:id', loadChildren: './company/company-detail/company-detail.module#CompanyDetailPageModule' },
  { path: 'list-companies', loadChildren: './company/list-companies/list-companies.module#ListCompaniesPageModule' },
  { path: 'dashboard', loadChildren: './user/dashboard/dashboard.module#DashboardPageModule' },
  { path: 'user-job-offers', loadChildren: './user/user-job-offers/user-job-offers.module#UserJobOffersPageModule' },
  { path: 'employer-job-offers', loadChildren: './user/employer-job-offers/employer-job-offers.module#EmployerJobOffersPageModule' },
  { path: 'add-user-job-offer/:id', loadChildren: './user-job-offer/add-user-job-offer/add-user-job-offer.module#AddUserJobOfferPageModule' },
  { path: 'register', loadChildren: './user/register/register.module#RegisterPageModule' },
  { path: 'settings', loadChildren: './user/settings/settings.module#SettingsPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
