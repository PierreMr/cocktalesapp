import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JobOffersService } from '../../services/job-offer/job-offers.service';
import { NavController } from '@ionic/angular';
import { JobOffer } from 'src/app/models/job-offer.interface';

@Component({
  selector: 'app-job-offer-detail',
  templateUrl: './job-offer-detail.page.html',
  styleUrls: ['./job-offer-detail.page.scss'],
})
export class JobOfferDetailPage implements OnInit {
  jobOffer;

  constructor(
    private jobOfferSrvc: JobOffersService,
    private activatedRoute: ActivatedRoute,
    private navCtrl: NavController,
  ) { 
    this.jobOfferSrvc.getJobOffer(Number(this.activatedRoute.snapshot.paramMap.get('id')))
    .then((jobOffer) => {
      this.jobOffer = jobOffer;
    }).catch(error => {
      console.log(error);
    });
  }

  ngOnInit() {
  }

  navCompanyDetail(id: number) {
    this.navCtrl.navigateForward('/company-detail/' + id);
  }

  applyJobOffer(jobOffer: JobOffer) {
    this.navCtrl.navigateForward('/add-user-job-offer/' + jobOffer.id);
  }

}
