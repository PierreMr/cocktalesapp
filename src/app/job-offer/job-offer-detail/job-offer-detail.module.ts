import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { JobOfferDetailPage } from './job-offer-detail.page';

const routes: Routes = [
  {
    path: '',
    component: JobOfferDetailPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [JobOfferDetailPage]
})
export class JobOfferDetailPageModule {}
