import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobOfferDetailPage } from './job-offer-detail.page';

describe('JobOfferDetailPage', () => {
  let component: JobOfferDetailPage;
  let fixture: ComponentFixture<JobOfferDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobOfferDetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobOfferDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
