import { Component, OnInit } from '@angular/core';
import { JobOffersService } from '../../services/job-offer/job-offers.service';
import { JobOffer } from '../../models/job-offer.interface';
import { NavController } from '@ionic/angular';
import { ControllersService } from '../../services/controllers/controllers.service';


@Component({
  selector: 'app-list-job-offers',
  templateUrl: './list-job-offers.page.html',
  styleUrls: ['./list-job-offers.page.scss'],
})
export class ListJobOffersPage implements OnInit {
  allJobOffers;
  jobOffers: JobOffer[] = [];

  constructor(
    private jobOfferSrvc: JobOffersService,
    private ctrlSrvc: ControllersService,
    private navCtrl: NavController,
  ) {
    this.ctrlSrvc.presentLoading('Chargement...')
    .then(() => {
      this.jobOfferSrvc.getJobOffers()
      .then((jobOffers: any) => {
        if (!jobOffers.error) {
          this.allJobOffers = jobOffers;
          this.ctrlSrvc.presentToast(`Il y a ${this.allJobOffers.length} annonces autour de vous.`, 3000, "primary");
          this.jobOffers = [...this.allJobOffers];
        } else {
          console.log(jobOffers.message);
        }
        this.ctrlSrvc.loadingCtrl.dismiss();
      }).catch(error => {
        console.log(error);
        this.ctrlSrvc.loadingCtrl.dismiss();
      });
    });
  }

  ngOnInit() { }

  doRefresh(event) {
    this.jobOfferSrvc.getJobOffers()
    .then((jobOffers: any) => {
      if (!jobOffers.error) {
        this.allJobOffers = jobOffers;
        this.jobOffers = [...this.allJobOffers];
      } else {
        console.log(jobOffers.message);
      }
      event.target.complete();
    }).catch(error => {
      console.log(error);
      event.target.complete();
    });
  }

  navJobOfferDetail(id) {
    this.navCtrl.navigateForward(['/job-offer-detail/'+id]);
  }

}
