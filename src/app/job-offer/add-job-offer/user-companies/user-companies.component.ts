import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ControllersService } from 'src/app/services/controllers/controllers.service';
import { UserService } from 'src/app/services/user/user.service';
import { api } from 'src/environments/environment';
import { Company } from 'src/app/models/company.interface';
import { JobOffersService } from 'src/app/services/job-offer/job-offers.service';

@Component({
  selector: 'app-user-companies',
  templateUrl: './user-companies.component.html',
  styleUrls: ['./user-companies.component.scss'],
})
export class UserCompaniesComponent implements OnInit {
  companies: any;

  constructor(
    private modalCtrl: ModalController,
    private ctrlSrvc: ControllersService,
    private userSrvc: UserService,
    private jobOffersSrvc: JobOffersService,
    ) {
    this.ctrlSrvc.presentLoading('')
    .then(() => {
      this.getCompanies()
      .then(companies => {
        this.companies = companies;
        this.ctrlSrvc.loadingCtrl.dismiss();
      }).catch(error => {
        console.log(error);
        this.ctrlSrvc.loadingCtrl.dismiss();
      });
    });
  }

  ngOnInit() {}

  dismiss() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  getCompanies() {
    return new Promise((resolve, reject) => {
      if (this.userSrvc.companies.length === 0) {
        const headers = new Headers();
        headers.append("Authorization", this.userSrvc.apiToken);
        const init = { 
          method: 'GET',
          headers: headers,
        };
        const url = api.url+'/company/user/'+this.userSrvc.user.id;
    
        fetch(url, init)
        .then(response => {
          response.json()
          .then(companies => {
            this.userSrvc.companies = companies;
            resolve(companies);
          }).catch(error => {
            console.log(error);
            reject(error);
          });
        }).catch(error => {
          console.log(error);
          reject(error);
        });
      }
      else {
        resolve(this.userSrvc.companies);
      }
    });
  }

  dismissCompany(company: Company) {
    this.modalCtrl.dismiss({
      'company': company
    });
  }
}
