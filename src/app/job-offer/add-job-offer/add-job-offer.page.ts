import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserService } from 'src/app/services/user/user.service';
import { NavController, ModalController } from '@ionic/angular';
import { ControllersService } from 'src/app/services/controllers/controllers.service';
import { Company } from 'src/app/models/company.interface';
import { api } from 'src/environments/environment';
import { UserCompaniesComponent } from 'src/app/job-offer/add-job-offer/user-companies/user-companies.component';

@Component({
  selector: 'app-add-job-offer',
  templateUrl: './add-job-offer.page.html',
  styleUrls: ['./add-job-offer.page.scss'],
})
export class AddJobOfferPage implements OnInit {
  addJobOfferForm: FormGroup;
  company: Company;

  constructor(
    private formBuilder: FormBuilder,
    private ctrlSrvc: ControllersService,
    private navCtrl: NavController,
    private modalCtrl: ModalController,
    private userSrvc: UserService,
  ) {
    this.addJobOfferForm = this.formBuilder.group({
      title: ['Intitulé de poste'],
      hourlyWadge: [13.6],
      description: [],
      city: [],
      company: []
    });
  }

  ngOnInit() {
  }

  async presentUserCompaniesModal() {
    const modal = await this.modalCtrl.create({
      component: UserCompaniesComponent
    });

    modal.onDidDismiss()
    .then((data) => {
      const modalData = data.data;
      if (modalData && modalData.company) {
        this.company = modalData.company;
        this.addJobOfferForm.patchValue({'company': modalData.company.id});
        this.addJobOfferForm.patchValue({'city': modalData.company.city});
      }
    }).catch(error => {
      console.log(error);
    });

    return await modal.present();
  }

  onSubmit(addJobOfferForm) {
    this.ctrlSrvc.presentLoading('Chargement...')
    .then(() => {
      const addJobOffer = addJobOfferForm.value;
      addJobOffer.status = true;
      addJobOffer.schedule = [
        {"startsAt": "2019-05-05T231245", "endsAt": "2019-05-06T164529"},
        {"startsAt": "2019-05-07T065213", "endsAt": "2019-05-08T175124"}
      ];

      const headers = new Headers();
      headers.append("Authorization", this.userSrvc.apiToken);
      const init = { 
        method: "POST",
        body: JSON.stringify(addJobOffer),
        headers: headers,
      };
      const url = api.url+'/joboffer/new';

      fetch(url, init)
      .then(response => {
        response.json()
        .then(jobOffer => {
          if (jobOffer.error) {
            this.ctrlSrvc.presentToast(`${jobOffer.message}`, 3000, 'danger');
          } else {
            if (this.userSrvc.companies.length !== 0) this.userSrvc.addJobOffer(jobOffer);
            this.ctrlSrvc.presentToast(`Annonce ajoutée.`, 3000, 'primary');
            this.navCtrl.navigateRoot('/dashboard');
          }
          this.ctrlSrvc.loadingCtrl.dismiss();
        }).catch(error => {
          console.log(error);
          this.ctrlSrvc.loadingCtrl.dismiss();
        });
      }).catch((error) => {
        console.log(error);
        this.ctrlSrvc.loadingCtrl.dismiss();
      })
    });
  }
}
