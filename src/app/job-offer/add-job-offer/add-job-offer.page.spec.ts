import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddJobOfferPage } from './add-job-offer.page';

describe('AddJobOfferPage', () => {
  let component: AddJobOfferPage;
  let fixture: ComponentFixture<AddJobOfferPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddJobOfferPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddJobOfferPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
