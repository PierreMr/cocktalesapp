import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUserJobOfferPage } from './add-user-job-offer.page';

describe('AddUserJobOfferPage', () => {
  let component: AddUserJobOfferPage;
  let fixture: ComponentFixture<AddUserJobOfferPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddUserJobOfferPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUserJobOfferPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
