import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JobOffersService } from 'src/app/services/job-offer/job-offers.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ControllersService } from 'src/app/services/controllers/controllers.service';
import { UserService } from 'src/app/services/user/user.service';
import { NavController } from '@ionic/angular';
import { api } from 'src/environments/environment';

@Component({
  selector: 'app-add-user-job-offer',
  templateUrl: './add-user-job-offer.page.html',
  styleUrls: ['./add-user-job-offer.page.scss'],
})
export class AddUserJobOfferPage implements OnInit {
  addUserJobOfferForm: FormGroup;
  jobOffer;

  constructor(
    private jobOfferSrvc: JobOffersService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private ctrlSrvc: ControllersService,
    private userSrvc: UserService,
    private navCtrl: NavController,
    ) {
    this.jobOfferSrvc.getJobOffer(Number(this.activatedRoute.snapshot.paramMap.get('id')))
    .then((jobOffer) => {
      this.jobOffer = jobOffer;
    }).catch(error => {
      console.log(error);
    });

    this.addUserJobOfferForm = this.formBuilder.group({
      comment: [],
    });
  }

  ngOnInit() {
  }

  onSubmit(addUserJobOfferForm) {
    this.ctrlSrvc.presentLoading('Chargement...')
    .then(() => {
      const addUserJobOffer = addUserJobOfferForm.value;
      addUserJobOffer.user = this.userSrvc.user.id;
      addUserJobOffer.jobOffer = this.jobOffer.id;

      const headers = new Headers();
      headers.append("Authorization", this.userSrvc.apiToken);
      const init = { 
        method: "POST",
        body: JSON.stringify(addUserJobOffer),
        headers: headers,
      };
      const url = api.url+'/usersjoboffer/new';

      fetch(url, init)
      .then(response => {
        response.json()
        .then(userJobOffer => {
          if (userJobOffer.error) {
            this.ctrlSrvc.presentToast(`${userJobOffer.message}`, 3000, 'danger');
          } else {
            if (this.userSrvc.userJobOffers.length !== 0) this.userSrvc.addUserJobOffer(userJobOffer);
            this.ctrlSrvc.presentToast(`Vous avez postulé une annonce.`, 3000, 'primary');
            this.navCtrl.navigateRoot('/dashboard');
          }
          this.ctrlSrvc.loadingCtrl.dismiss();
        }).catch(error => {
          console.log(error);
          this.ctrlSrvc.loadingCtrl.dismiss();
        });
      }).catch((error) => {
        console.log(error);
        this.ctrlSrvc.loadingCtrl.dismiss();
      })
    });
  }

}
