import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { api } from 'src/environments/environment';
import { CompaniesService } from 'src/app/services/company/companies.service';
import { UserService } from 'src/app/services/user/user.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-company-detail',
  templateUrl: './company-detail.page.html',
  styleUrls: ['./company-detail.page.scss'],
})
export class CompanyDetailPage implements OnInit {
  company;
  segment: string = "detail";
  rates;

  constructor(
    private companiesSrvc: CompaniesService,
    private activatedRoute: ActivatedRoute,
    private userSrvc: UserService,
    private navCtrl: NavController,
  ) {
    this.companiesSrvc.getCompany(Number(this.activatedRoute.snapshot.paramMap.get('id')))
    .then((company) => {
      this.company = company;
      
      this.getCompanyRates()
      .then(rates => {
        this.rates = rates;
      }).catch(error => {
        console.log(error);
      });
    }).catch(error => {
      console.log(error);
    });
  }

  ngOnInit() {
  }

  segmentChanged(e: any) {
    this.segment = e.detail.value;
  }

  getCompanyRates() {
    return new Promise((resolve, reject) => {
      if (!this.rates) {
        const headers = new Headers();
        headers.append("Authorization", this.userSrvc.apiToken);
        const init = { 
          method: 'GET',
          headers: headers,
        };
        const url = api.url+'/companyrate/company/'+this.company.id;
    
        fetch(url, init)
        .then(response => {
          response.json()
          .then(rates => {
            resolve(rates);
          }).catch(error => {
            console.log(error);
          });
        }).catch(error => {
          console.log(error);
        });
      }
      else {
        resolve(this.rates);
      }
    });
  }

}
