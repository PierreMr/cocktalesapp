import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { CompaniesService } from 'src/app/services/company/companies.service';
import { ControllersService } from 'src/app/services/controllers/controllers.service';
import { Company } from 'src/app/models/company.interface';

@Component({
  selector: 'app-list-companies',
  templateUrl: './list-companies.page.html',
  styleUrls: ['./list-companies.page.scss'],
})
export class ListCompaniesPage implements OnInit {
  allCompanies;
  companies: Company[];

  constructor(
    private companiesSrvc: CompaniesService,
    private ctrlSrvc: ControllersService,
    private navCtrl: NavController,
  ) {
    this.companiesSrvc.getCompanies()
    .then((companies) => {
      this.allCompanies = companies;
      this.ctrlSrvc.presentToast(`Il y a ${this.allCompanies.length} établissements autour de vous.`, 3000, "primary");
      this.companies = [...this.allCompanies];
    }).catch(error => {
      console.log(error);
    });
  }

  ngOnInit() {
  }

  doRefresh(event) {
    this.companiesSrvc.getCompanies()
    .then(companies => {
      this.allCompanies = companies;
      this.companies = [...this.allCompanies];
      event.target.complete();
    }).catch(error => {
      console.log(error);
      event.target.complete();
    });
  }

  navCompanyDetail(id) {
    this.navCtrl.navigateForward(['/company-detail/'+id]);
  }

  onCompanyChange(input: string): void {
    const companyInput = input.toLowerCase();
    this.companies = this.allCompanies.filter(company =>
        company.name.toLowerCase().includes(companyInput)
    );
  }

  onCompanyCancel() {
    this.companies = this.allCompanies;
  }

}
