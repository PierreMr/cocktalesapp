import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss'],
})
export class AddressComponent implements OnInit {
  addresses: any[]

  constructor(
    private modalCtrl: ModalController,
  ) { }

  ngOnInit() {}

  onAddressChange(input: string): void {
    const addressInput = input.toLowerCase();
    fetch('https://nominatim.openstreetmap.org/search?format=json&q='+addressInput)
    .then(response => {
      response.json()
      .then(addresses => {
        this.addresses = addresses;
      }).catch(error => {
        console.log(error);
      });
    }).catch(error => {
      console.log(error);
    });
  }

  dismissAddress(address: any) {
    this.modalCtrl.dismiss({
      'address': address
    });
  }
}
