import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { api } from 'src/environments/environment';
import { ControllersService } from 'src/app/services/controllers/controllers.service';
import { NavController, ModalController } from '@ionic/angular';
import { UserService } from 'src/app/services/user/user.service';
import { AddressComponent } from './address/address.component';

@Component({
  selector: 'app-add-company',
  templateUrl: './add-company.page.html',
  styleUrls: ['./add-company.page.scss'],
})
export class AddCompanyPage implements OnInit {
  addCompanyForm: FormGroup;
  companyTypes: {} = [];

  constructor(
    private formBuilder: FormBuilder,
    private ctrlSrvc: ControllersService,
    private navCtrl: NavController,
    private userSrvc: UserService,
    private modalCtrl: ModalController,
  ) {
    this.getCompanyTypes().then(companyTypes => {
      this.companyTypes = companyTypes;
    }).catch(error => {
      console.log(error);
    });

    this.addCompanyForm = this.formBuilder.group({
      name: ['CockTalesBar'],
      companyType: [1],
      address: [],
      zip: [],
      city: [],
      theme: [],
      phone: [],
      email: [],
      website: [],
      description: [],
      image: ['default.png'],
      lat: [],
      lng: [],
      user: [this.userSrvc.user.id]
    });
  }

  ngOnInit() {
  }

  getCompanyTypes() {
    return new Promise((resolve, reject) => {
      const headers = new Headers();
      headers.append("Authorization", this.userSrvc.apiToken);
      const init = { 
        method: 'GET',
        headers: headers,
      };
      const url = api.url+'/companytype';
  
      fetch(url, init)
      .then(response => {
        response.json()
        .then(companyTypes => {
          resolve(companyTypes);
        }).catch(error => {
          console.log(error);
        })
      }).catch(error => {
        console.log(error);
      })
    });
  }

  async presentAddressModal() {
    const modal = await this.modalCtrl.create({
      component: AddressComponent
    });

    modal.onDidDismiss()
    .then((data) => {
      const modalData = data.data;
      if (modalData && modalData.address) {
        fetch('https://nominatim.openstreetmap.org/reverse?format=json&lat='+ modalData.address.lat + '&lon=' + modalData.address.lon)
        .then(response => {
          response.json()
          .then(addr => {
            const address = addr.address;
            if (address.city) this.addCompanyForm.patchValue({'city': address.city});
            if (address.postcode) this.addCompanyForm.patchValue({'zip': address.postcode});
            if (address.road) {
              let street = '';
              if (address.house_number) street += address.house_number+', ';
              street += address.road;
              this.addCompanyForm.patchValue({'address': street});
            }
            if (addr.lat) this.addCompanyForm.patchValue({'lat': addr.lat});
            if (addr.lon) this.addCompanyForm.patchValue({'lng': addr.lon});
          }).catch(error => {
            console.log(error);
          });
        }).catch(error => {
          console.log(error);
        });
      }
    }).catch(error => {
      console.log(error);
    });

    return await modal.present();
  }

  onSubmit(addCompanyForm) {
    const addCompany = addCompanyForm.value;

    const headers = new Headers();
    headers.append("Authorization", this.userSrvc.apiToken);
    const init = { 
      method: "POST",
      body: JSON.stringify(addCompany),
      headers: headers,
    };
    const url = api.url+'/company/new';

    fetch(url, init)
    .then(response => {
      response.json()
      .then(company => {
        if (company.error) {
          this.ctrlSrvc.presentToast(`${company.message}`, 3000, 'danger');
        } else {
          if (this.userSrvc.companies.length !== 0) this.userSrvc.addCompany(company);
          this.ctrlSrvc.presentToast(`Etablissement ajouté.`, 3000, 'primary');
          this.navCtrl.navigateRoot('/dashboard');
        }
      }).catch(error => {
        console.log(error);
      });
    }).catch((error) => {
      console.log(error);
    });
  }

}
