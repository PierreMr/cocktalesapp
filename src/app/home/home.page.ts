import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { UserService } from '../services/user/user.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(
    private navCtrl: NavController,
    private userSrvc: UserService,
  ) {}

  navSettings() {
    this.navCtrl.navigateForward('/settings');
  }

  navLogin() {
    this.navCtrl.navigateForward('/login');
  }

  navRegister() {
    this.navCtrl.navigateForward('/register');
  }
}
