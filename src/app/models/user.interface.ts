export interface User {
    id: number,
    firstName: string,
    lastName: string,
    email: string,
    phone: string,
    address: string,
    zip: string,
    city: string,
    apiToken: string,
    image: string,
    roles: string[],
    username: string,
    createdAt: Date,
    updatedAt: Date,
}