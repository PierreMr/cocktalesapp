import { Company } from './company.interface';

export interface JobOffer {
    id: number,
    title: string,
    company: Company,
    schedule: string[],
    hourlyWadge: number,
    status: boolean,
    description: string,
    city: string,
    createdAt: Date,
    updatedAt: Date,
}