import { CompanyType } from './company-type.interface';

export interface Company {
    id: number,
    name: string,
    type: CompanyType,
    address: string,
    zip: string,
    city: string,
    lat: number,
    lng: number,
    theme: string,
    phone: string,
    email: string,
    website: string,
    description: string,
    createdAt: Date,
    updatedAt: Date,
}