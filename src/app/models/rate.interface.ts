export interface Rate {
    id: number,
    rate: number,
    comment: string,
    createdAt: Date,
    updatedAt: Date,
}