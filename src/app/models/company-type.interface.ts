export interface CompanyType {
    id: number,
    label: string,
    createdAt: Date,
    updatedAt: Date,
}