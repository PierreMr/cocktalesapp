import { Injectable } from '@angular/core';
import { Company } from '../../models/company.interface';
import { UserService } from '../user/user.service';
import { api } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CompaniesService {
  companies: Company[] = [];
  company: Company;

  constructor(
    private userSrvc: UserService,    
  ) { }

  getCompanies() {
    return new Promise((resolve, reject) => {
      if (this.companies.length === 0) {
        const headers = new Headers();
        headers.append("Authorization", this.userSrvc.apiToken);
        const init = { 
          method: 'GET',
          headers: headers,
        };
        const url = api.url+'/company';
    
        fetch(url, init)
        .then(response => {
          response.json()
          .then(companies => {
              this.companies = companies;
              resolve(this.companies);
          }).catch(error => {
            console.log(error);
            reject(error);
          });
        }).catch(error => {
          console.log(error);
          reject(error);
        });
      }
      else {
        resolve(this.companies);
      }
    });
  }

  getCompany(id: number) {
    return new Promise((resolve, reject) => {
      if (this.companies.length === 0) {
        const headers = new Headers();
        headers.append("Authorization", this.userSrvc.apiToken);
        const init = { 
          method: 'GET',
          headers: headers,
        };
        const url = api.url+'/company/'+id;
    
        fetch(url, init)
        .then(response => {
          response.json()
          .then(company => {
            resolve(company);
          }).catch(error => {
            console.log(error);
            reject(error);
          });
        }).catch(error => {
          console.log(error);
          reject(error);
        });
      }
      else {
        resolve(this.companies.find(company => company.id === id));
      }
    });
  }

  addCompany(company: Company) {
    this.companies = [...this.companies, company];
  }
}
