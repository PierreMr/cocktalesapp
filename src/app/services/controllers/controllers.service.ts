import { Injectable } from '@angular/core';
import { ToastController, LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ControllersService {
  loadingCtrl;

  constructor(
    private toastCtrl: ToastController,
    private loadingController: LoadingController,
  ) {
    this.loadingCtrl = loadingController;
  }

  async presentToast(message: string, duration: number, color: string) {
    const toast = await this.toastCtrl.create({
      message,
      duration,
      color,
      buttons: [
        {
          icon: "close",
          role: "cancel",
          side: "end",
        }
      ]
    });
    toast.present();
  }

  async presentLoading(message: string) {
    const loading = await this.loadingController.create({
      spinner: 'dots',
      message,
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }
}
