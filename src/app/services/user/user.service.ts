import { Injectable } from '@angular/core';
import { User } from '../../models/user.interface';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';
import { Company } from '../../models/company.interface';
import { JobOffer } from 'src/app/models/job-offer.interface';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  user: User;
  apiToken: string = "";
  dashboardToggle = "employee";
  companies: Company[] = [];
  jobOffers: JobOffer[] = [];
  userJobOffers: any[] = [];

  constructor(
    private storage: Storage,
    private navCtrl: NavController,    
  ) { }

  login(user: any) {
    this.user = user;
    this.apiToken = user.apiToken;

    this.storage.set('user', user);
    this.storage.set('apiToken', user.apiToken);
    this.storage.set('connected', true);
  }

  logout() {
    this.user = undefined;
    this.apiToken = "";

    this.storage.remove('user');
    this.storage.remove('apiToken');
    this.storage.remove('connected');

    this.navCtrl.navigateRoot('/home');
  }

  addCompany(company: Company) {
    this.companies = [...this.companies, company];
  }

  addJobOffer(jobOffer: JobOffer) {
    this.jobOffers = [...this.jobOffers, jobOffer];
  }

  addUserJobOffer(userJobOffer) {
    this.userJobOffers = [...this.userJobOffers, userJobOffer];
  }
}
