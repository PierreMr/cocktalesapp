import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';
import { JobOffer } from '../../models/job-offer.interface';
import { UserService } from '../user/user.service';
import { api } from 'src/environments/environment';
import { Company } from '../../models/company.interface';

@Injectable({
  providedIn: 'root'
})
export class JobOffersService {
  jobOffers: JobOffer[] = [];
  company: Company;
 
  constructor(
    private navCtrl: NavController,
    private userSrvc: UserService,
  ) { 
  }

  getJobOffers() {
    return new Promise((resolve, reject) => {
      if (this.jobOffers.length === 0) {
        const headers = new Headers();
        headers.append("Authorization", this.userSrvc.apiToken);
        const init = { 
          method: 'GET',
          headers: headers,
        };
        const url = api.url+'/joboffer';
    
        fetch(url, init)
        .then(response => {
          response.json()
          .then(jobOffers => {
              this.jobOffers = jobOffers;
              resolve(this.jobOffers);
          }).catch(error => {
            console.log(error);
            reject(error);
          });
        }).catch(error => {
          console.log(error);
          reject(error);
        });
      }
      else {
        resolve(this.jobOffers);
      }
    });
  }

  getJobOffer(id: number) {
    return new Promise((resolve, reject) => {
      if (this.jobOffers.length === 0) {
        const headers = new Headers();
        headers.append("Authorization", this.userSrvc.apiToken);
        const init = { 
          method: 'GET',
          headers: headers,
        };
        const url = api.url+'/joboffer/'+id;
    
        fetch(url, init)
        .then(response => {
          response.json()
          .then(jobOffer => {
            resolve(jobOffer);
          })
        })
      }
      else {
        resolve(this.jobOffers.find(jobOffer => jobOffer.id === id));
      }
    });
  }

  addJobOffer(jobOffer) {
    this.jobOffers = [...this.jobOffers, jobOffer];
  }
}
